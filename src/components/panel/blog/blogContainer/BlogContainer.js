import React, { useEffect, useState } from "react";
import Header from "../../../Header/Header";
import Pagination from "../../../../tools/Pagination/Pagination";
import Box from "../../../../tools/box/Box";
import EditIcon from "@material-ui/icons/Edit";
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import { BaseUrl, GetAllContentsRequest, GetAllProductsRequest } from "../../../../api/Url";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import { PersionCurrency, PersionDate } from "../../../../tools/Tools";
import Loading from "../../../../tools/Loading/Loading";
import AddCircleOutlineIcon from '@material-ui/icons//AddCircleOutline';
import { IconButton } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
const BlogContainer = () => {
  useEffect(() => {
    const getdata = async () => {
      const data = await GetAllContent();
      data.reverse();
      var ProductList = [];
      for (var i = 0; i < data.length; i++) {
        var obj = {};
        obj["id"] = data[i].id;
        obj["title"] = data[i].title;
        let Pd = await PersionDate(data[i].createdDatetime);
        obj["date"] = Pd;
        ProductList.push(obj);
      }
      setContents(ProductList);
      setpreload(false);
    };
    getdata();
  }, []);
  const [Contents, setContents] = useState();
  const [preload, setpreload] = useState(true);
  const GetAllContent = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetAllContentsRequest,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  };

  return (
    <>
      <Header title="مدیریت محتوا" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              <Link to="/panel/content/add">
                <IconButton aria-label="primary" color="primary" size="large">
                  <AddCircleOutlineIcon fontSize="inherit" />
                </IconButton></Link>

              <TableHeader items={["کد مقاله", "تیتر", "تاریخ"]} />
              <Pagination
                perpage="10"
                datas={Contents}
                link="/panel/content/edit?id="
              />
            </>
          }
        />
      )}
      <ToastContainer />
    </>
  );
};

export default BlogContainer;
