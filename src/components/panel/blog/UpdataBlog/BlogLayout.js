import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../../tools/AccessDenied/Accessdenied";
import Container from "../../../LayoutContainers/Container";

import Nav from "../../../Navbar/Nav";
import AddBlog from "./AddBlog";
import UpdateBlog from "./UpdateBlog";

const BlogLayout = (props) => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      {UserState.Data.accessLevel !== 3 ?
        <>
          <Nav />
          <Container
            child={props.action === "add" ? <AddBlog /> : <UpdateBlog />}
          /> </>
        : <Accessdenied />

      }

    </>
  );
};

export default BlogLayout;
