import { Button, FormControl, InputLabel, Link, TextareaAutosize } from "@material-ui/core";
import { MenuItem, Select, TextField } from "@material-ui/core";
import ColorPicker from "material-ui-color-picker";
import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  BaseUrl,
  DeleteProductRequest,
  GetCollectionsRequest,
  GetContentRequest,
  GetProductRequest,
  GetWriterRequest,
  UpdateContentRequest,
  UpdateProductRequest,
  UploadFileRequest,
} from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import Header from "../../../Header/Header";
import "./UpdateProduct.css";
import 'react-toastify/dist/ReactToastify.css';
import QuestionBox from "../../../../tools/QuestionBox/QuestionBox";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Asset from '../../../../images/logo.png'
const UpdateBlog = () => {
  const [Writers, setWriters] = useState();
  const [BlogContent, setBlogContent] = useState();
  const [preload, setpreload] = useState(true);
  const [ContentTitle, setContentTitle] = useState('');
  const [ContentTitleEn, setContentTitleEn] = useState('');

  const [ContentWriter, setContentWriter] = useState('');
  const [ContentCategory, setContentCategory] = useState('');
  const [ContentDescription, setContentDescription] = useState('');
  const [ContentDescriptionEn, setContentDescriptionEn] = useState('');
  const [Imagesid, setImagesid] = useState(0);
  const [Image, setImage] = useState(Asset);
  const [ImageParagraph, setImageParagraph] = useState(Asset);
  const [Paragraph, setParagraph] = useState([]);
  useEffect(() => {
    const GetData = async () => {
      const search = window.location.search; // could be '?foo=bar'
      const params = new URLSearchParams(search);
      // bar
      var id = params.get("id");
      var Data = GetContent(id);
      var WriterData = await GetWriters();
      setWriters(WriterData);
      setBlogContent(Data);
      setpreload(false);
    }
    GetData()
  }, []);
  const ChangeTitle = (e) => {
    setContentTitle(e.target.value);
  }
  const ChangeEnglishTitle = (e) => {
    setContentTitleEn(e.target.value);
  }

  const handleCollection = (e) => {
    console.log(e.target.value);
    setContentWriter(e.target.value);

  }
  const handleType = (e) => {
    setContentCategory(e.target.value);
  }
  const ChangeDescription = (e) => {
    setContentDescription(e.target.value);
  }
  const ChangeDescriptionEn = (e) => {
    setContentDescriptionEn(e.target.value);
  }

  const GetWriters = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetWriterRequest,
      raw,
      myHeaders,
      "GET"
    );

    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  };
  const AddContentAction = async () => {
    console.log("hi0");
    var raw = JSON.stringify({
      "id": 0,
      "title": ContentTitle,
      "titleen": ContentTitleEn,
      "paragraphs": Paragraph,
      "lables": ContentCategory,
      "userid": ContentWriter,
      "contentimageid": Imagesid
    });
    console.log(raw);
    if (ContentTitle.length > 0 && ContentWriter.length > 0 && ContentCategory.length > 0 && Imagesid > 0) {
      setpreload(true);

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        "id": 0,
        "title": ContentTitle,
        "titleen": ContentTitleEn,
        "paragraphs": Paragraph,
        "lables": ContentCategory,
        "userid": ContentWriter,
        "contentimageid": Imagesid
      });
      console.log(raw);
      const result = await CallApi(
        BaseUrl + UpdateContentRequest,
        raw,
        myHeaders,
        "POST"
      );
      if (result[1] === 200) {
        setpreload(false);
        /*    dispatch(Success(result[0].data)); */
        toast.success('محصول با موفقیت ثبت شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });

        window.location.replace("/panel/Contents/edit?id=" + result[0].data.id);
      }
      else {
        toast.error('خطا در ثبت محصول', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setpreload(false);
      }
    }
    else {
      toast.error('فیلد های خواسته شده را وارد کنید', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }


  }
  const ChangeImage = async (e) => {
    setpreload(true);
    var myHeaders = new Headers();
    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);
    try {
      const result = await CallApi(
        BaseUrl + UploadFileRequest,
        formdata,
        myHeaders,
        "POST"
      );
      /* console.log(result) */
      if (result[1] === 200) {

        setImagesid(result[0].data.id);
        /*     element.src=result[0].data.filePath; */

        setpreload(false);
        setImage(result[0].data.filePath);
        toast.success('عکس با موفقیت بارگذاری شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      else {
        setpreload(false);
        toast.error('خطا در آپلود عکس', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setpreload(false);
      toast.error('خطا در آپلود عکس', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }
  const ChangeParagraphPic = async (e) => {

    setpreload(true);
    var myHeaders = new Headers();
    /*  myHeaders.append("Content-Type", "application/json"); */

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);


    try {
      const result = await CallApi(
        BaseUrl + UploadFileRequest,
        formdata,
        myHeaders,
        "POST"
      );
      /* console.log(result) */
      if (result[1] === 200) {
        setImageParagraph(result[0].data.filePath);
        /*     element.src=result[0].data.filePath; */

        setpreload(false);

        toast.success('عکس بارگذاری شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      else {
        setpreload(false);
        toast.error('خطا در آپلود عکس', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setpreload(false);
      toast.error('خطا در آپلود عکس', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }
  const AddPatagraph = () => {
    if(ContentDescription !=="" && ContentDescriptionEn!==""){
      var ParagraphList = Paragraph;
      var obj = {};
      obj["picpath"] = ImageParagraph;
      obj["description"] = ContentDescription;
      obj["descriptionen"] = ContentDescriptionEn;
      ParagraphList.push(obj);
      setContentDescriptionEn("");
      setContentDescription("");
      setImageParagraph(Asset);
      setParagraph(ParagraphList);
      toast.success('پاراگراف جدید اضافه شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }else{
      toast.error('اطلاعات پاراگراف را کامل اضافه کنید', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  
    /* console.log(ParagraphList.length); */
  }
  const GetContent =async (id)=>{
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetContentRequest + "?id=" + id,
      raw,
      myHeaders,
      "GET"
    );

    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  }
  return <>
    <Header title="افزودن مقاله" />
    {preload === true ? <Loading /> :
      <Box
        child={
          <>
            <div className="flex-center center-row rtl">
              <UploadAsset src={Image} title="عکس اصلی" accept="image/*" id="PrimaryImage" Change={(e) => ChangeImage(e, "PrimaryImage", 0)} />

            </div>

            <div className="row-fields flex-center">
              <TextField
                value={ContentTitle}
                required
                label="عنوان مقاله "
                onChange={(e) => ChangeTitle(e)}
              />
              <TextField
                value={ContentTitleEn}
                required
                label="عنوان انگلیسی مقاله "
                onChange={(e) => ChangeEnglishTitle(e)}
              />
              <FormControl
                variant="standard"
                sx={{ m: 1, minWidth: 160 }}
                required
              >
                <InputLabel id="WriterLable">نام نویسنده</InputLabel>
                <Select
                  labelId="WriterLable"
      
                  label="نویسنده"
                  onChange={(e) => handleCollection(e)}
                >
                  {Writers.map((item) => (
                    <MenuItem value={item.id}>{item.name + " " + item.family}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl
                variant="standard"
                sx={{ m: 1, minWidth: 160 }}
                required
              >
                <InputLabel id="TypeLable">نام دسته بندی</InputLabel>
                <Select
                  labelId="TypeLable"
                  label="نوع محصول"
                  onChange={(e) => handleType(e)}
                >
                  <MenuItem value={"scarf"}>اسکارف</MenuItem>
                  <MenuItem value={"shawl"}>شال</MenuItem>
                  <MenuItem value={"headgear"}>روسری</MenuItem>
                </Select>
              </FormControl>
            </div>

            {Paragraph.length > 0 ?
              Paragraph.map((item) => (
                <>
                  <div className="row-fields center-row">
                    <UploadAsset src={item.picpath} title="عکس پاگراف" />
                    <TextareaAutosize
                      value={item.description}
                      labelId="Description"
                      aria-label="minimum height"
                      minRows={5}
                      placeholder="توضیحات"
                      style={{ width: 480 }}

                    />
                  </div>
                </>

              ))
              : null}


            <div className="row-fields center-row">
              <UploadAsset src={ImageParagraph} title="عکس پاگراف" accept="image/*" id="PrimaryImage" Change={(e) => ChangeParagraphPic(e)} />
            </div>
            <div className="row-fields center-row">

              <TextareaAutosize
                value={ContentDescription}
                labelId="Description"
                aria-label="minimum height"
                minRows={5}
                placeholder="توضیحات"
                style={{ width: 480 }}
                onChange={(e) => ChangeDescription(e)}
              />
              <TextareaAutosize
                value={ContentDescriptionEn}
                labelId="Description"
                aria-label="minimum height"
                minRows={5}
                placeholder="توضیحات انگلیسی"
                style={{ width: 480 }}
                onChange={(e) => ChangeDescriptionEn(e)}
              />
            </div>
            <div className="row-fields center-row">
              <Button onClick={AddPatagraph} variant="contained">ثبت پاراگراف</Button>
            </div>

            <div className="row-fields">
              {/* <ColorPicker
                  name="color"
                  defaultValue="#000"
                  // value={this.state.color} - for controlled component
                  onChange={(color) => console.log(color)}
                /> */}
            </div>
            <div className="row-fields">
              <Button onClick={AddContentAction} variant="contained">ثبت محصول </Button>

            </div>
            <ToastContainer />


          </>
        }

      />
    }
  </>;
    
};

export default UpdateBlog;
