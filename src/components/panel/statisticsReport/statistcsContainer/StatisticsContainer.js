import React, { useEffect, useState } from 'react';
import Header from "../../../Header/Header";
import LineChart from '../../../../tools/LineChart/LineChart';
import VerticalChart from '../../../../tools/VerticalChart/VerticalChart';
import { CallApi } from '../../../../tools/CallApi/CallApi';
import { BaseUrl, GetProductSalesSratistics, GetDailySalesStatistics } from '../../../../api/Url';

const StatisticsContainer = () => {

    const [salesSratistics, setSalesSratistics] = useState();
    const [dailySales, setDailySales] = useState();
    const [labelSales, setLabelSales] = useState();
    const [loader, setLoader] = useState(true);
    const [loader_2, setLoader_2] = useState(true);

    useEffect(()=> {
        
        const fetching = async () => {
            const ls = window.localStorage.getItem("UserToken");
            const token = JSON.parse(ls).token
            var myHeaders = new Headers();
            myHeaders.append("Authorization", `Bearer ${token}`);
            const resolve = await CallApi(`${BaseUrl+GetProductSalesSratistics}`, "{}", myHeaders, "GET" );
            const titleResovle = resolve[0].data;
            setLabelSales(titleResovle.map(item => item.title))
            setSalesSratistics(resolve[0].data);
            setLoader(false);
        }

        const fetchingDaily = async () => {
            const ls = window.localStorage.getItem("UserToken");
            const token = JSON.parse(ls).token;
            var myHeaders = new Headers();
            myHeaders.append("Authorization", `Bearer ${token}`);
            const resolve = await CallApi(`${BaseUrl+GetDailySalesStatistics}`, "{}", myHeaders, "GET" );
            console.log(resolve[0].data);
            const dataCalled = resolve[0].data.reverse();
            setDailySales(dataCalled)
            setLoader_2(false);
        }

        fetching();
        fetchingDaily();

    }, [])

    return (
        <>
          <Header title="گزارشات مالی" />
          {
            !loader_2 && <LineChart titleProps={"گزارشات تراکنش ها"} reportProps={"سقف تراکنش های روزانه"} reportProps2={" سقف تعداد فروش روزانه"} dataCalled={dailySales} />
          }
          {
            !loader && <VerticalChart reportProps={"گزارش فروش هر شال"} dataApi={salesSratistics} labelsPro={labelSales} />
          }
        </>
    );
};

export default StatisticsContainer;