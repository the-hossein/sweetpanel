import React from "react";
import Container from "../../../LayoutContainers/Container";

import Nav from "../../../Navbar/Nav";
import AddCollection from "./AddCollection";

import UpdateCollection from "./UpdateCollection";

const CollectionLayout = (props) => {
  return (
    <>
      <Nav />

      <Container
        child={
          props.action === "add" ? <AddCollection /> : <UpdateCollection />
        }
      />
    </>
  );
};

export default CollectionLayout;
