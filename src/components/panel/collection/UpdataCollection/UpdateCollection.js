import { Button, FormControl, InputLabel, TextareaAutosize } from "@material-ui/core";
import { MenuItem, Select, TextField } from "@material-ui/core";
import ColorPicker from "material-ui-color-picker";
import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  BaseUrl,
  GetCollectionRequest,
  GetProductRequest,
  UpdateCollectionRequest,
  UploadFileRequest,
} from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Header from "../../../Header/Header";
import Asset from "../../../../images/logo.png"
const UpdateCollection = () => {
  /*  const [product, setproduct] = useState(); */
  const [preload, setpreload] = useState(true);
  const [CollectionId, setCollectionId] = useState();
  const [CollectionTitle, setCollectionTitle] = useState('');
  const [CollectionColor, setCollectionColor] = useState('');
  const [CollectionTitleEn, setCollectionTitleEn] = useState('');
  const [Image, setImage] = useState();
  const [Imagesid, setImagesid] = useState();
  const [CollectionDescription, setCollectionDescription] = useState('');
  const [CollectionDescriptionEn, setCollectionDescriptionEn] = useState('');


  useEffect(() => {
    const Loader = async () => {
      const search = window.location.search; // could be '?foo=bar'
      const params = new URLSearchParams(search);
      // bar
      var id = params.get("id");

      var C = await GetCollection(id);
      console.log(C);
      setCollectionId(C.id)
      setCollectionTitle(C.title)
      setCollectionTitleEn(C.titleEn)
      setCollectionDescription(C.description)
      setCollectionDescriptionEn(C.descriptionEn)
      setCollectionColor(C.colorCode)
      if(C.picture !== null){
        setImagesid(C.picture.id)
        setImage(C.picture.filePath)
      }
      else{
        setImage(Asset);
      }
      setpreload(false);
    };
    Loader();
  }, []);
  const GetCollection = async (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    console.log(BaseUrl + GetProductRequest + "?id=" + id);
    const result = await CallApi(
      BaseUrl + GetCollectionRequest + "?id=" + id,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      return result[0].data;
    }
  };
  const ChangeImage = async (e, id, index) => {
    setpreload(true);
    var myHeaders = new Headers();
    /*  myHeaders.append("Content-Type", "application/json"); */

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);


    try {
      const result = await CallApi(
        BaseUrl + UploadFileRequest,
        formdata,
        myHeaders,
        "POST"
      );
      /* console.log(result) */
      if (result[1] === 200) {
        setImagesid(result[0].data.id);
        setpreload(false);
        setImage(result[0].data.filePath);
        toast.success('عکس با موفقیت ثبت شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      else {
        setpreload(false);
        toast.error('خطا در آپلود عکس', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setpreload(false);
      toast.error('خطا در آپلود عکس', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }
  const ChangeDescription = (e) => {
    setCollectionDescription(e.target.value)
  }
  const ChangeDescriptionEn = (e) => {
    setCollectionDescriptionEn(e.target.value)
  }
  const ChangeTitle = (e) => {
    setCollectionTitle(e.target.value)
  }
  const ChangeTitleEn = (e) => {
    setCollectionTitleEn(e.target.value)
  }
  const ChangeColor = (e) => {
    setCollectionColor(e.target.value)
  }
  const UpdateCollectionAction = async () => {
    setpreload(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    console.log(Imagesid);
    var raw = JSON.stringify({
      "id": CollectionId,
      "title": CollectionTitle,
      "titleen": CollectionTitleEn,
      "colorcode":CollectionColor,
      "picid":Imagesid,
      "description": CollectionDescription,
      "descriptionen": CollectionDescriptionEn,
    });
   
    
    const result = await CallApi(
      BaseUrl + UpdateCollectionRequest,
      raw,
      myHeaders,
      "POST"
    );
    console.log(result);
    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      setpreload(false);
      toast.success('با موفقیت ثبت شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

    }
    else {
      toast.error('خطا در انجام عملیات', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }
  return (
    <>
      <Header title="به روز رسانی کالکشن" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          child={
            <>
              <div className="flex-center center-row rtl">
                <UploadAsset src={Image} title="عکس اصلی" accept="image/*" Change={(e) => ChangeImage(e, "PrimaryImage", 0)} />

              </div>
              <div className="row-fields">
                <TextField
                  value={CollectionTitle}
                  required
                  label="عنوان کالکشن "
                  onChange={(e) => ChangeTitle(e)}
                />
                <TextField
                  value={CollectionTitleEn}
                  required
                  label="عنوان انگلیسی کالکشن "
                  onChange={(e) => ChangeTitleEn(e)}
                />
                <TextField
                  value={CollectionColor}
                  required
                  label="کد رنگ "
                  className="ltr-input"
                  onChange={(e) => ChangeColor(e)}
                />
              </div>
              <div className="row-fields center-row">
                <TextareaAutosize
                  value={CollectionDescription}
                  labelId="Description"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات"
                  style={{ width: 480 }}
                  onChange={(e) => ChangeDescription(e)}
                />
                <TextareaAutosize
                  value={CollectionDescriptionEn}
                  labelId="Description"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات انگلیسی"
                  style={{ width: 480 }}
                  onChange={(e) => ChangeDescriptionEn(e)}
                />
              </div>
              <div className="row-fields">
                <Button onClick={UpdateCollectionAction} variant="contained">به روز رسانی</Button>
               {/*  <Button onClick={OpemPopUp} className="error-btn" variant="outlined" color="error">
                  حذف محصول
                </Button> */}
              </div>
            </>
          }
        />
      )}
      <ToastContainer />
    </>
  );
};

export default UpdateCollection;
