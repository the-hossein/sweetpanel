import React from "react";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import CollectionContainer from "./collectionContainer/collectionContainer";

const Collections = () => {
  return (
    <>
      <Nav />
      <Container child={<CollectionContainer />} />
    </>
  );
};

export default Collections;
