import React, { useEffect, useState } from "react";
import Header from "../../../Header/Header";
import Pagination from "../../../../tools/Pagination/Pagination";
import Box from "../../../../tools/box/Box";
import EditIcon from "@material-ui/icons/Edit";
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import {
  BaseUrl,
  GetAllProductsRequest,
  GetCollectionsRequest,
} from "../../../../api/Url";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import { PersionCurrency, PersionDate } from "../../../../tools/Tools";
import Loading from "../../../../tools/Loading/Loading";
import AddCircleOutlineIcon from '@material-ui/icons//AddCircleOutline';
import { Link } from "react-router-dom";
import { IconButton } from "@material-ui/core";
const CollectionContainer = () => {
  useEffect(() => {
    const getdata = async () => {
      const data = await GetAllCollection();
      data.reverse();
      /*  console.log(data[0].title); */
      var ProductList = [];
      for (var i = 0; i < data.length; i++) {
        var obj = {};
        obj["id"] = data[i].id;
        obj["title"] = data[i].title;
        obj["colorcode"] = data[i].colorCode;

        ProductList.push(obj);
      }
      setProducts(ProductList);
      setpreload(false);
    };
    getdata();
  }, []);
  const [Products, setProducts] = useState();
  const [preload, setpreload] = useState(true);
  const GetAllCollection = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetCollectionsRequest,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  };

  return (
    <>
      <Header title="مدیریت کالکشن" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          table ={true}
          child={
            <>
               <Link to="/panel/collection/add">
                <IconButton aria-label="primary" color="primary" size="large">
                  <AddCircleOutlineIcon fontSize="inherit" />
                </IconButton></Link>
              <TableHeader items={["کد کالکشن", "نام", "رنگ"]} />
              <Pagination
                perpage="3"
                datas={Products}
                link="/panel/collection/edit?id="
              />
            </>
          }
        />
      )}
    </>
  );
};

export default CollectionContainer;
