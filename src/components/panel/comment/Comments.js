import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../tools/AccessDenied/Accessdenied";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import CommentContainer from "./commentContainer/CommentContainer";


const Comments = () => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      <Nav />
      <Container child={UserState.Data.accessLevel !== 3 && UserState.Data.accessLevel !== 2 && UserState.Data.accessLevel !== 1 ? <CommentContainer /> : <Accessdenied />} />
    </>
  );
};

export default Comments;
