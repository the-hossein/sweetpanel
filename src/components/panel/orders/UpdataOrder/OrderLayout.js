import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../../tools/AccessDenied/Accessdenied";
import Container from "../../../LayoutContainers/Container";

import Nav from "../../../Navbar/Nav";
import SendingOrder from "./SendingOrder";
import UpdateOrder from "./UpdateOrder";


const OrderLayout = (props) => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      {UserState.Data.accessLevel !== 3 ?
        <>
          <Nav />
          <Container
            child={props.action === "SendState" ? <SendingOrder /> : <UpdateOrder />}
          /> </>
        : <Accessdenied />

      }

    </>
  );
};

export default OrderLayout;
