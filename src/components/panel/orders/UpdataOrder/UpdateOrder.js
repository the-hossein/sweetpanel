

import Button from '@mui/material/Button';
import React, { useEffect, useRef, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  BaseUrl, ChangepayStateRequest, CloseBasketRequest, GetBasketByIdRequest, GetBasketDetailsRequest,
} from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import Header from "../../../Header/Header";
import "./UpdateProduct.css";
import 'react-toastify/dist/ReactToastify.css';
import Pagination from "../../../../tools/Pagination/Pagination";
import QRCode from 'react-qr-code';
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import { PersionCurrency } from "../../../../tools/Tools";
import Row from "../../../../tools/tableRow/Row";
import { useReactToPrint } from 'react-to-print';
import { ComponentToPrint } from './PrintToComponent';
import { PrintOrder } from './PrintOrder';
const UpdateOrder = () => {
  const [details, setdetails] = useState();
  const [factore, setfactore] = useState();
  const [preload, setpreload] = useState(true);
  const [Bid, setBId] = useState();

  useEffect(() => {
    const Loader = async () => {
      const search = window.location.search; // could be '?foo=bar'
      const params = new URLSearchParams(search);
      // bar
      var id = params.get("id");
      setBId(id);
      const data = await GetDetails(id);
      const facoredata = await GetFactorDetail(id);
    
      setfactore(facoredata);
      var BasketDetails = [];
      for (var i = 0; i < data.length; i++) {
        var obj = {};
        obj["id"] = data[i].productId;
        obj["title"] = data[i].title;
        obj["amount"] = (await PersionCurrency(data[i].amount)) + "تومان";
        obj["qty"] = data[i].qty;
        BasketDetails.push(obj);
      }

      setdetails(BasketDetails);
      setpreload(false);

    };
    Loader();
  }, []);
  const GetDetails = async (id) => {
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetBasketDetailsRequest + "?id=" + id,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      return result[0].data;
    }
  };
  const GetFactorDetail = async (id) => {
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetBasketByIdRequest + "?id=" + id,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      return result[0].data;
    }
  };
  const UpdateBasketDetail = async (stateId) => {
    setpreload(true);
    var myHeaders = new Headers();
    /* var token = JSON.parse(localStorage.getItem("UserToken")); */
    myHeaders.append("Content-Type", "application/json");
    /*  myHeaders.append("Authorization", `Bearer ${token.token}`); */
    console.log(stateId);
    var raw = JSON.stringify({
      "id": Bid,
      "basketstate": stateId
    });
    const result = await CallApi(
      BaseUrl + ChangepayStateRequest,
      raw,
      myHeaders,
      "POST"
    );
    if (result[1] === 200) {

      /*  if(stateId === 4){
         handlePrint()
       } */
      factore.state = stateId;
      setfactore(factore);
      setpreload(false);
      toast.success('سفارش به روز رسانی شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    else {
      setpreload(false);
      toast.error('خطا در به روزرسانی', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }
  const CloeseBasket = async (BasketId) => {
    setpreload(true);
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);

    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + CloseBasketRequest + "?BasketId=" + BasketId,
      raw,
      myHeaders,
      "POST"
    );
    console.log(BaseUrl + CloseBasketRequest + "?BasketId=" + BasketId);
    if (result[1] === 200) {
      factore.state = 6;
      setfactore(factore);
      setpreload(false);
      toast.success('سفارش به روز رسانی شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    else {
      setpreload(false);
      toast.error('خطا در به روزرسانی', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }
  
  const componentRef = useRef();
  const handlePrint = useReactToPrint({

    content: () => componentRef.current,
  });
  return (
    <>

      <Header title="جزئیات سفارش" />
      {preload === true ? (
        <Loading />
      ) : (

        <>

          <Box child={
            <>
              <PrintOrder qrlink={"https://panel.tiolastyle.com/panel/order/UpdateSendState?id=" + Bid} factore={factore} ref={componentRef} />
              <Box
                table={true}
                child={
                  <>
                    <TableHeader items={["کد محصول", "نام محصول", "قیمت", "تعداد"]} />
                    {details.map((item) =>
                      <Row

                        data={Object.values(item)}

                        link={false}
                      />
                    )
                    }

                    <ToastContainer />


                  </>
                }
              />

              <div className="row-fields">

                {factore.state === 0 ? <Button onClick={(e) => UpdateBasketDetail(4)} variant="contained">تایید برای بسته بندی</Button> : null}
                <Button variant="contained" onClick={handlePrint}>Print</Button>


              </div>
            </>
          } />
        </>

      )}
    </>
  );
};

export default UpdateOrder;

