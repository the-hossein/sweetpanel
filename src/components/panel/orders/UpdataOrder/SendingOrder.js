

import Button from '@mui/material/Button';
import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  BaseUrl, ChangepayStateRequest, CloseBasketRequest, GetBasketByIdRequest, GetBasketDetailsRequest,
} from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import Header from "../../../Header/Header";
import "./UpdateProduct.css";
import 'react-toastify/dist/ReactToastify.css';
import Pagination from "../../../../tools/Pagination/Pagination";
import QRCode from 'react-qr-code';
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import { PersionCurrency } from "../../../../tools/Tools";
import Row from "../../../../tools/tableRow/Row";
import { TextField } from '@material-ui/core';
const SendingOrder = () => {
  const [details, setdetails] = useState();
  const [factore, setfactore] = useState();
  const [preload, setpreload] = useState(true);
  const [Bid, setBId] = useState();
  const [shipingCode, setshipingCode] = useState();
  const [UpdateState, setUpdateState] = useState(false);

  useEffect(() => {
    const Loader = async () => {
      const search = window.location.search; // could be '?foo=bar'
      const params = new URLSearchParams(search);
      // bar
      var id = params.get("id");
      setBId(id);
      const data = await GetDetails(id);
      const facoredata = await GetFactorDetail(id);
      /* setfactore(facoredata); */

      var BasketDetails = [];
      for (var i = 0; i < data.length; i++) {
        var obj = {};
        obj["id"] = data[i].productId;
        obj["title"] = data[i].title;
        obj["qty"] = data[i].qty;
        BasketDetails.push(obj);
      }
      if (facoredata.state === 0) {
        window.location = "/panel/order/edit?id=" + facoredata.id
      }
      else if (facoredata.state === 4) {
        UpdateBasketDetail(5, id);
        facoredata.state = 5;
        setUpdateState(true);
      }
      setfactore(facoredata);
      setdetails(BasketDetails);
      setpreload(false);

    };
    Loader();
  }, []);
  const GetDetails = async (id) => {
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetBasketDetailsRequest + "?id=" + id,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      return result[0].data;
    }
  };
  const GetFactorDetail = async (id) => {
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetBasketByIdRequest + "?id=" + id,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      return result[0].data;
    }
  };
  const UpdateBasketDetail = async (stateId, BasketId) => {

    var myHeaders = new Headers();
    /* var token = JSON.parse(localStorage.getItem("UserToken")); */
    myHeaders.append("Content-Type", "application/json");
    /*  myHeaders.append("Authorization", `Bearer ${token.token}`); */

    var raw = JSON.stringify({
      "id": BasketId,
      "basketstate": stateId
    });
    console.log(raw);
    const result = await CallApi(
      BaseUrl + ChangepayStateRequest,
      raw,
      myHeaders,
      "POST"
    );
    if (result[1] === 200) {
      factore.state = 5;
      setfactore(factore);

      toast.success('سفارش به روز رسانی شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    else {

      toast.error('خطا در به روزرسانی', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }
  const CloeseBasket = async (BasketId) => {
    setpreload(true);
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);

    var raw = JSON.stringify({});
    if (shipingCode.length > 0) {
      const result = await CallApi(
        BaseUrl + CloseBasketRequest + "?BasketId=" + BasketId + "&PostCode=" + shipingCode,
        raw,
        myHeaders,
        "POST"
      );

      if (result[1] === 200) {
        factore.state = 6;
        setfactore(factore);
        setpreload(false);
        toast.success('سفارش به روز رسانی شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      else {
        setpreload(false);
        toast.error('خطا در به روزرسانی', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } else {
      setpreload(false);
      toast.error('کد مرسوله را وارد کنید', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }


  }
  const ChangeShipingCode = (e) => {
    setshipingCode(e.target.value)
  }
  return (
    <>
      <Header title="به روز رسانی سفارش" />
      {preload === true ? (
        <Loading />
      ) : (

        <>
          <Box child={
            <>
              <div className="factore-data">
                <h2>{`کد فاکتور : ${factore.id}`}</h2>
                <h3>{`نام مشتری : ${factore.user.name} ${factore.user.family}`}</h3>
                <p>{`شماره تماس : ${factore.phoneNumber}`}</p>
                <p>{`آدرس : ${factore.address.address}`}</p>
                <p>{`روش ارسال  : ${factore.shiping === 0 ? "پست پیشتاز " : "پیک سفارشی"}`}</p>
                <p>{`وضعیت سفارش : ${factore.state === 0 ? "در انتظار تایید برای بسته بندی" : factore.state === 4 ? "در حال بسته بندی" : factore.state === 5 ? "ارسال برای مشتری" : factore.state === 6 ? "سفارش بسته شده است" : "سفارش تایید نشده"} `}</p>
              </div>
              <Box
                table={true}
                child={
                  <>
                    <TableHeader items={["کد محصول", "نام محصول", "تعداد"]} />
                    {details.map((item) =>
                      <Row
                        data={Object.values(item)}
                        link={false}
                      />
                    )}
                    <ToastContainer />
                  </>
                }
              />
              {/*  <QRCode
                title="Tiola"
                value="https://tiolastyle.com/"
                bgColor="#FFFFFF"
                fgColor="#000000"
                size={128}
              /> */}
              <div className="row-fields">
                {factore.state === 5 && UpdateState === false ?
                  <><TextField
                    value={shipingCode}
                    required
                    label="کد مرسوله"
                    onChange={(e) => ChangeShipingCode(e)}
                  />
                    <Button onClick={(e) => CloeseBasket(factore.id)} variant="contained">افزودن کد پستی</Button> </>
                  : UpdateState === true ? <Button variant="contained" color="success">با موفقیت ثبت شد</Button> : null}



                {/*  <Button onClick={UpdateProductAction} variant="contained">تایید</Button>
                 <Button onClick={OpemPopUp} className="error-btn" variant="outlined" color="error">
                   حذف محصول
                 </Button> */}
              </div>
            </>
          } />
        </>

      )}
    </>
  );
};

export default SendingOrder;
