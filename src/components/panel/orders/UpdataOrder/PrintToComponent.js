import React from "react";
import QRCode from "react-qr-code";
import { Link } from "react-router-dom";
import Box from "../../../../tools/box/Box";
import Header from "../../../Header/Header";

import UpdateOrder from "./UpdateOrder";

export class ComponentToPrint extends React.PureComponent {

    render() {
        return (
            <>
                {console.log(this.props.factore)}
                <Box table={true} child={
                    this.props.factore.map((item) => (
                        <>
                            <div className="factore-data">
                                <div className="factore-detail">
                                    <p>{`کد فاکتور : ${item.id}`}</p>
                                    <p>{`نام مشتری : ${item.name}   `}</p>
                                    <p>{` شماره تماس : ${item.phone} `}</p>
                                    
                                </div>
                                <p>{`آدرس : ${item.address}`}</p>
                                <p>{`کد پستی : ${item.postCode}`}</p>
                                <div className='qrcode'>
                                    <QRCode
                                        title="Tiola"
                                        value={"https://panel.tiolastyle.com/panel/order/UpdateSendState?id=" + item.id}
                                        bgColor="#FFFFFF"
                                        fgColor="#000000"
                                        size={100}
                                    />
                                </div>
                            </div>
                        </>
                    ))
                }
                />


            </>

        );
    }
}