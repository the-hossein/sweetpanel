import React, { useEffect, useRef, useState } from "react";
import Header from "../../../Header/Header";
import Pagination from "../../../../tools/Pagination/Pagination";
import Box from "../../../../tools/box/Box";
import EditIcon from "@material-ui/icons/Edit";
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import { BaseUrl, ChangepayStateRequest, GetAllProductsRequest, GetOrderRequest, GetSuccessOrderWithStateRequest } from "../../../../api/Url";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import { PersionCurrency, PersionDate } from "../../../../tools/Tools";
import Loading from "../../../../tools/Loading/Loading";
import AddCircleOutlineIcon from '@material-ui/icons//AddCircleOutline';
import { IconButton } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { Button, TextField } from "@mui/material";
import { ComponentToPrint } from "../UpdataOrder/PrintToComponent";
import { useReactToPrint } from "react-to-print";
const OrderContainer = (props) => {
  useEffect(() => {
    const getdata = async () => {
      var data = [];
      if (props.state === "Manager") {
        data = await GetAllOrder();
      } else {
        data = await GetStateOfAllOrder(props.state);
      }
      data.reverse();
      console.log(data);
      var ProductList = [];
      if (props.state === 0) {
        for (var i = 0; i < data.length; i++) {
          var obj = {};
          obj["id"] = data[i].basket.id;
          obj["name"] = data[i].user.name + " " + data[i].user.family
          obj["phone"] = data[i].user.phoneNumber
          obj["address"] = data[i].basket.address.address
          obj["postCode"] = data[i].basket.address.postCode
          let Pd = await PersionDate(data[i].createdDatetime);
          obj["date"] = Pd;
          obj["state"] = data[i].basket.state === 0 ? <span className="waiting">در انتظار پردازش</span> : data[i].basket.state === 4 ? <span className="processing">در حال پردازش</span> : data[i].basket.state === 5 ? <span className="sending">ارسال به مشتری</span> : data[i].basket.state === 6 ? <span className="finishing">تحویل به مشتری</span> : "وضعیت نامعتبر";
          ProductList.push(obj);
        }
      } else {
        for (var i = 0; i < data.length; i++) {
          var obj = {};
          obj["id"] = data[i].basket.id;
          obj["name"] = data[i].user.name + " " + data[i].user.family
          obj["phone"] = data[i].user.phoneNumber
          obj["price"] = (await PersionCurrency(data[i].amount/10)) + "تومان";

          let Pd = await PersionDate(data[i].createdDatetime);
          obj["date"] = Pd;
          obj["state"] = data[i].basket.state === 0 ? <span className="waiting">در انتظار پردازش</span> : data[i].basket.state === 4 ? <span className="processing">در حال پردازش</span> : data[i].basket.state === 5 ? <span className="sending">ارسال به مشتری</span> : data[i].basket.state === 6 ? <span className="finishing">تحویل به مشتری</span> : "وضعیت نامعتبر";
          ProductList.push(obj);
        }
      }

      setProducts(ProductList);
      setorders(ProductList);
      setpreload(false);
    };
    getdata();
  }, [props]);
  const [Products, setProducts] = useState();
  const [orders, setorders] = useState();
  const [preload, setpreload] = useState(true);
  const GetAllOrder = async () => {
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetOrderRequest,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  };
  const GetStateOfAllOrder = async (stateid) => {
    var myHeaders = new Headers();
    var token = JSON.parse(localStorage.getItem("UserToken"));
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token.token}`);
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetSuccessOrderWithStateRequest + "?state=" + stateid,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  };
  const SearchByName = (e) => {
    var search = Products.filter(a => a.name.includes(e.target.value));
    setorders(search);
  }
  const UpdateBasketDetail = async (id) => {
    var myHeaders = new Headers();
    /* var token = JSON.parse(localStorage.getItem("UserToken")); */
    myHeaders.append("Content-Type", "application/json");
    /*  myHeaders.append("Authorization", `Bearer ${token.token}`); */

    var raw = JSON.stringify({
      "id": id,
      "basketstate": 4
    });
    const result = await CallApi(
      BaseUrl + ChangepayStateRequest,
      raw,
      myHeaders,
      "POST"
    );
    if (result[1] === 200) {
      console.log(result[0]);
      /*  if(stateId === 4){
         handlePrint()
       } */

    }
    else {

    }

  }
  const UpdateStates = async () => {
    setpreload(true)
    for (var i = 0; i < orders.length; i++) {
      UpdateBasketDetail(orders[i].id)
    }
    setpreload(false)
    handlePrint()
  }
  const componentRef = useRef();
  const handlePrint = useReactToPrint({

    content: () => componentRef.current,
  });
  return (
    <>
      <Header title="مدیریت سفارشات" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              {/* <Link to="/panel/products/add">
                <IconButton aria-label="primary" color="primary" size="large">
                  <AddCircleOutlineIcon fontSize="inherit" />
                </IconButton></Link> */}


              <TextField
                id="outlined-basic"
                onChange={(e) => SearchByName(e)}
                variant="outlined"
                fullWidth
                label="Search"
              />

              {props.state === 0 ?
                <>
                  <ComponentToPrint factore={orders} ref={componentRef} />
                  <Button variant="contained" onClick={UpdateStates}>Print</Button></> :
                <>
                  <TableHeader items={["کد سفارش", "نام مشتری", "شماره تماس ", "قیمت", "تاریخ", "وضعیت سفارش"]} />
                  <Pagination
                    perpage={10}
                    datas={orders}
                    link="/panel/order/edit?id="
                  />
                </>
              }
            
            </>
          }
        />
      )}
      <ToastContainer />
    </>
  );
};

export default OrderContainer;
