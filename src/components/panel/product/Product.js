import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../tools/AccessDenied/Accessdenied";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import ProductContainer from "./productContainer/ProductContainer";

const Product = () => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      <Nav />
      <Container child={UserState.Data.accessLevel !== 3 ? <ProductContainer /> : <Accessdenied/>} />
    </>
  );
};

export default Product;
