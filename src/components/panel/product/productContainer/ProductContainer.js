import React, { useEffect, useState } from "react";
import Header from "../../../Header/Header";
import Pagination from "../../../../tools/Pagination/Pagination";
import Box from "../../../../tools/box/Box";
import EditIcon from "@material-ui/icons/Edit";
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import { BaseUrl, GetAllProductsRequest } from "../../../../api/Url";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import { PersionCurrency, PersionDate } from "../../../../tools/Tools";
import Loading from "../../../../tools/Loading/Loading";
import AddCircleOutlineIcon from '@material-ui/icons//AddCircleOutline';
import { IconButton } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { TextField } from "@mui/material";
const ProductContainer = () => {
  useEffect(() => {
    const getdata = async () => {
      const data = await GetAllProduct();
      data.reverse();
      var ProductList = [];
      for (var i = 0; i < data.length; i++) {
        var obj = {};
        obj["id"] = data[i].id;
        obj["title"] = data[i].title;
        obj["price"] = (await PersionCurrency(data[i].price)) + "تومان";
        let Pd = await PersionDate(data[i].createdDatetime);
        obj["date"] = Pd;
        ProductList.push(obj);
      }
      setProducts(ProductList);
      setFilterProducts(ProductList);
      setpreload(false);
    };
    getdata();
  }, []);
  const [Products, setProducts] = useState();
  const [FilterProducts, setFilterProducts] = useState();
  const [preload, setpreload] = useState(true);
  const GetAllProduct = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetAllProductsRequest,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  };
  const SearchByName = (e) => {
    var search = Products.filter(a => a.title.includes(e.target.value) || a.type.includes(e.target.value));
    setFilterProducts(search);
  }
  return (
    <>
      <Header title="مدیریت محصولات" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              <TextField
                id="outlined-basic"
                onChange={(e) => SearchByName(e)}
                variant="outlined"
                fullWidth
                label="Search"
              />
              <Link to="/panel/products/add">
                <IconButton aria-label="primary" color="primary" size="large">
                  <AddCircleOutlineIcon fontSize="inherit" />
                </IconButton>
              </Link>

              <TableHeader items={["کد محصول", "نام", "قیمت", "تاریخ"]} />
              <Pagination
                perpage={10}
                datas={FilterProducts}
                link="/panel/products/edit?id="
              />
            </>
          }
        />
      )}
      <ToastContainer />
    </>
  );
};

export default ProductContainer;
