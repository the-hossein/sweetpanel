import React, { useEffect, useState } from 'react'
import Style from "./DashboardStatistics.module.css"
import PeopleIcon from '@material-ui/icons/People';
import StoreIcon from '@material-ui/icons/Store';
import BlockIcon from '@material-ui/icons/Block';
import ChatIcon from '@material-ui/icons/Chat';
import { CallApi } from "../../../../tools/CallApi/CallApi";
import { BaseUrl, GetCommentCountRequest, GetProductCountRequest, GetvUserAcountCountRequest } from '../../../../api/Url';
import Loading from '../../../../tools/Loading/Loading';
const DashboardStatistics = () => {
    const [ProdoctCount, setProdoctCount] = useState(0);
    const [userCount, setuserCount] = useState(0);
    const [CommentCount, setCommentCount] = useState(0);
    const [Preload, setPreload] = useState(true);
    useEffect(() => {
        const setAllDate =async () => {
            var _ProductCount = await GetProductCount();
            var _UserCount = await GetUserCount();
            var _CommentCount = await GetCommentCount();
            setProdoctCount(_ProductCount)
            setuserCount(_UserCount)
            setCommentCount(_CommentCount)
            setPreload(false)
        }
        setAllDate()
    }, []);
    const GetProductCount = async () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({});
        const result = await CallApi(
            BaseUrl + GetProductCountRequest,
            raw,
            myHeaders,
            "GET"
        );
        console.log(result);
        if (result[1] === 200) {
            /*    dispatch(Success(result[0].data)); */
            return result[0].data;
        }
    }
    const GetCommentCount = async () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({});
        const result = await CallApi(
            BaseUrl + GetCommentCountRequest,
            raw,
            myHeaders,
            "GET"
        );
        console.log(result);
        if (result[1] === 200) {
            /*    dispatch(Success(result[0].data)); */
            return result[0].data;
        }
    }
    const GetUserCount = async () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({});
        const result = await CallApi(
            BaseUrl + GetvUserAcountCountRequest,
            raw,
            myHeaders,
            "GET"
        );
        console.log(result);
        if (result[1] === 200) {
            /*    dispatch(Success(result[0].data)); */
            return result[0].data;
        }
    }

    return (
        <>
            {Preload === true ? <Loading /> : <div className={Style.Statistic_Item}>
                <div>
                    <div>
                        <PeopleIcon />
                        <h3>کاربران فعال</h3>
                        <p>{userCount}</p>
                    </div>

                </div>
                <div>
                    <div>
                        <StoreIcon />
                        <h3>محصولات</h3>
                        <p>{ProdoctCount}</p>
                    </div>

                </div>
                <div>
                    <div>
                        <ChatIcon />
                        <h3>تعداد نظرات کاربران</h3>
                        <p>{CommentCount}</p>
                    </div>
                </div>
                <div>
                    <div>
                        <BlockIcon />
                        <h3>فایل های تایید نشده</h3>
                        <p>0</p>
                    </div>
                </div>
            </div>}

        </>
    )
}

export default DashboardStatistics