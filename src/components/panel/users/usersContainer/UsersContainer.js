import React, { useEffect, useState } from "react";
import Header from "../../../Header/Header";
import Pagination from "../../../../tools/Pagination/Pagination";
import Box from "../../../../tools/box/Box";
import EditIcon from "@material-ui/icons/Edit";
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import { BaseUrl, GetAllProductsRequest, GetAllUserActiceRequest, GetAllUserActiveRequest } from "../../../../api/Url";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import { PersionCurrency, PersionDate } from "../../../../tools/Tools";
import Loading from "../../../../tools/Loading/Loading";
import AddCircleOutlineIcon from '@material-ui/icons//AddCircleOutline';
import { IconButton } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { TextField } from "@mui/material";
const UsersContainer = () => {
  useEffect(() => {
    const getdata = async () => {
      const data = await GetAllUsers();
      data.reverse();
      var ProductList = [];
      for (var i = 0; i < data.length; i++) {
        var obj = {};
        obj["fullname"] = data[i].fullName;
        obj["basketCount"] = data[i].basketCount;
        obj["phoneNumber"] = data[i].phoneNumber;
        let Pd = await PersionDate(data[i].birthDay);
        obj["birthDay"] = Pd;
        ProductList.push(obj);
      }
      setUsers(Users);
      setFilterUsers(ProductList);
      setpreload(false);
    };
    getdata();
  }, []);
  const [Users, setUsers] = useState();
  const [FilterUsers, setFilterUsers] = useState();
  const [preload, setpreload] = useState(true);
  const GetAllUsers = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + GetAllUserActiveRequest,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      return result[0].data;
    }
  };
  const SearchByName = (e) => {
    var search = Users.filter(a => a.fullname.includes(e.target.value));
    setFilterUsers(search);
  }
  return (
    <>
      <Header title="مدیریت کاربران" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              <TextField
                id="outlined-basic"
                onChange={(e) => SearchByName(e)}
                variant="outlined"
                fullWidth
                label="Search"
              />
              <Link to="/panel/products/add">
                <IconButton aria-label="primary" color="primary" size="large">
                  <AddCircleOutlineIcon fontSize="inherit" />
                </IconButton>
              </Link>

              <TableHeader items={["نام کاربر", "تعداد خرید", "شماره تماس", "تاریخ تولد"]} />
              <Pagination
                perpage={10}
                datas={FilterUsers}
                link="/panel/products/edit?id="
              />
            </>
          }
        />
      )}
      <ToastContainer />
    </>
  );
};

export default UsersContainer;
