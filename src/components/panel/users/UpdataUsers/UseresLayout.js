import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../../tools/AccessDenied/Accessdenied";
import Container from "../../../LayoutContainers/Container";

import Nav from "../../../Navbar/Nav";
import AddProduct from "./AddProduct";
import UpdateProduct from "./UpdateProduct";
const UseresLayout = (props) => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      {UserState.Data.accessLevel !== 3 ?
        <>
          <Nav />
          <Container
            child={props.action === "add" ? <AddProduct /> : <UpdateProduct />}
          /> </>
        : <Accessdenied />

      }

    </>
  );
};

export default UseresLayout;
