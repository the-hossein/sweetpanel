import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import "./Row.css";

const Row = (props) => {
  /*   useEffect(() => {
      alert("rowww")
      }, []); */
  return (
    <>
      {props.data.length > 0 ? !props.link ? <div className="tb-row">
        {props.data.map((item) => (
          <div className="tb-column">
            <p>{item}</p>
          </div>
        ))}
        {/*   <div className="edit-row">{props.editbtn}</div> */}
      </div> : <Link to={props.link} className="tb-row">
        {props.data.map((item) => (
          <div className="tb-column">
            <p>{item}</p>
          </div>
        ))}
        {/*   <div className="edit-row">{props.editbtn}</div> */}
      </Link> : <a className="no-exist">
        <p>موردی  یافت نشد</p>
      </a>}

    </>
  );
};

export default Row;
