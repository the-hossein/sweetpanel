import { Grid  } from "@material-ui/core";
import React from "react";
import warning from "../../images/warning.png";
import './QuestionBox.css'
const QuestionBox = (props) => {
 
  return (
    <div className="exit-box">
      <Grid container spacing={2}>
       
          <div className="exit-mesasge ">
            <div className="title-exit">
              <div>
                <img src={warning} />
              </div>
              <h2>{props.title}</h2>
            </div>
            <p>{props.warning}</p>
            <div className="redirect-btn mt-5">
              {props.submit}
              {props.cancel}
            </div>
          </div>
        </Grid>
      
    </div>
        
   
    
  );
};

export default QuestionBox;
