import React from "react";
import "./TableHeader.css";
const TableHeader = (props) => {
  return (
    <>
      <div className="tb-head">
        {props.items.map((item) => (
          <div className="tb-column">
            <p>{item}</p>
          </div>
        ))}
      
      </div>
    </>
  );
};

export default TableHeader;
