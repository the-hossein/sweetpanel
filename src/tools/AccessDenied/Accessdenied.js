import React from 'react'
import Header from '../../components/Header/Header'
import Container from '../../components/LayoutContainers/Container'
import Nav from '../../components/Navbar/Nav'
import Box from '../box/Box'

const Accessdenied = () => {
    return (
        <>
            <Header title="عدم سطح دسترسی" />
        </>
    )
}

export default Accessdenied