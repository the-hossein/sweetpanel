import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import style from './LineChart.module.css';
// import faker from 'faker';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);


const LineChart = ({ titleProps, reportProps, reportProps2, dataCalled }) => {

    const options = {
      responsive: true,
      scales: {
        x: {
          grid: {
            display: false,
          },
        },
        y: {
          grid: {
            display: false,
          },
        },
      },
      plugins: {
          legend: {
            position: 'top' ,
            labels:{
                  usePointStyle: true,
                  font:{
                    size:12,
                    family: "IRANSansWeb"
                }
            }
        },
        title: {
          display: true,
          FontFace:"Ir",
          text: titleProps,
          font:{
              size: 16,
              family: "IRANSansWeb"
          }
        },
        tooltip: {
            font:{
            },
            bodyFont: {
                size:14 ,
                family: "IRANSansWeb",
            },
        }
    
      },
    };
    
    const getDate = (date) => {
      const roundDate = date.split("T");
      return roundDate[0];
    };

    const roundPay = (pay) => {
      const roundedPay = pay / 10;
      return roundedPay;
    }

    const labels = dataCalled.map(item => getDate(item.dateTime));
    
    const data = {
      labels,
      datasets: [
        {
          lineTension: .2,
          label: reportProps,
          data: dataCalled.map(item => roundPay(item.amount) ),
          borderColor: 'rgb(62, 199, 11)',
          backgroundColor: 'rgba(62, 199, 11)',
          borderRadius:20,
          
        },
        {
            lineTension: .2,
            label: reportProps2,
            data: dataCalled.map(item => item.count),
            borderColor: 'rgb(255, 210, 76)',
            backgroundColor: 'rgba(255, 210, 76)',
            
        }
      ],
    };

    return (
        <div className={style.mainChart}>
            <Line options={options} data={data} />
        </div>
    );
};

export default LineChart;