import React from "react";
import "./Box.css";
const Box = (props) => {
  return (
    <>
    {props.table === true ?  <div className="box-tbl">{props.child}</div> :  <div className="box">{props.child}</div>}
    </>
  );
};

export default Box;
