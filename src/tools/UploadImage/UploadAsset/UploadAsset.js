import { IconButton } from "@material-ui/core";
import { PhotoCamera } from "@material-ui/icons";
import React, { } from "react";
import Asset from '../../../images/logo.png'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import "./UploadAsset.css";
const UploadAsset = (props) => {

  return (
    <div className="edit-asset-item">
      {props.removeIcon !== false ? <RemoveCircleOutlineIcon onClick={props.removeIcon} /> : null}
      <img src={props.src} alt="" />
      <div>
        <div className="edit-img-profile mt-1">
          <input
            type="file"
            className="custom-file-input"
            onChange={props.Change}
            multiple
            accept={props.accept}
          />
          <IconButton color="primary" aria-label="upload picture" component="span">
            <PhotoCamera />
          </IconButton>
        </div>
      </div>

      <p>{props.title}</p>
    </div>
  );
}


export default UploadAsset;
