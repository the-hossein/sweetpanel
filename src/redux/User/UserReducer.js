

const initiailState = {
  Data: {},
  LoginState: false,
  Loading : false
};

const UserReducer = (State = initiailState, Action) => {
  switch (Action.type) {
    case "Request":
     return{
       ...State,
       Loading:true,
       LoginState:false
     }

    case "Success":
      
      return {
        ...State,
        Data: Action.payload,
        LoginState:true,
        Loading:false
      };
    case "Faild":return{
      ...State,
     
      LoginState:false,
      
    }
    default:
      return State;
  }
};
export default UserReducer;
