import "./App.css";
import { Route, Routes } from "react-router-dom";
import Dashboard from "./components/panel/dashboard/Dashboard";
import Product from "./components/panel/product/Product";
import Login from "./components/panel/login/Login";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { GetUserData } from "./redux/User/UserAction";
import Loading from "./tools/Loading/Loading";
import AddProduct from "./components/panel/product/UpdataProduct/AddProduct";
import ProductLayout from "./components/panel/product/UpdataProduct/ProductLayout";

function App() {
  const dispath = useDispatch();
  const UserState = useSelector((state) => state.User)
  const [preload, setpreload] = useState(true);
  useEffect(() => {
    if (localStorage.getItem("UserToken")) {
      
      var token = JSON.parse(localStorage.getItem("UserToken"))
      const now = new Date();
      const endDate = new Date(token.expiration);
      if (endDate - now < 0) {
        localStorage.removeItem("UserToken");
        localStorage.removeItem("UserPhoneNumber");

        if (window.location.pathname !== "/") {
          window.location = "/"
        }
        else {
          setpreload(false)
        }
      } else {
       
        if (!UserState.LoginState) {
       
          dispath(GetUserData(localStorage.getItem("UserPhoneNumber")));
          setpreload(false)
        }
        else {
          if (UserState.Data.accessLevel !== 3) {

            if (window.location.pathname !== "/") {
              window.location = "/"
            }
          }
          else {
            setpreload(false)
          }
        }
      }

    } else {
      console.log("inja");
      /*   console.log(window.location.pathname); */
      if (window.location.pathname !== "/") {
        window.location = "/"
      }
      else {
        setpreload(false)
      }
    }
  },

    []);
  return (
    <div className="App">
      {preload === true ? <Loading /> : <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/panel/dashboard" element={<Dashboard />} />
        <Route path="/panel/products" element={<Product />} />
        <Route path="/panel/products/add" element={<ProductLayout action="add"/>} />
        <Route path="/panel/products/edit" element={<ProductLayout action="edit"/>} />
      </Routes>}

    </div>
  );
}

export default App;
